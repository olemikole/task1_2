#include <iostream>
#include <limits>
int main()
{
    int square_size;
    float row_col_relation = 1.8; //This number will depend on the terminal
    int width;

    while( true )
    { 
        std::cout<<"Enter a positive integer:"<<std::endl;
        std::cin >> square_size;
        if( !std::cin.fail() && square_size >= 0)
            break;
        
        std::cout<<"Bad Input"<<std::endl<<std::endl;

        //Clears error bit
        std::cin.clear();

        //Clears input buffer until newline
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n'); 
    }
    width = square_size*row_col_relation;
    
    for(int i=0; i<square_size; i++)
    {
        if( i==0 || i==square_size-1)
        {
            for(int j=0; j<width; j++)
            {
                std::cout << "*";
            }
        }
        else
        {
            for(int j=0; j<width; j++)
            {
                if(j==0 || j==width-1)
                    std::cout << "*";
                else
                    std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}